import { FTPClient } from "https://deno.land/x/ftpc@v1.2.1/mod.ts";

const clientId = Deno.env.get("CLIENT_ID");
const clientIntegrationId = Deno.env.get("CLIENT_INTEGRATION_ID");
const clientEcommerce = JSON.parse(Deno.env.get("CLIENT_ECOMMERCE"));
const ftpHost = Deno.env.get("FTP_HOST");
const user = Deno.env.get("FTP_USER");
const pass = Deno.env.get("FTP_PASS");
const articleFilepath = Deno.env.get("FTP_ARTICLES_FILEPATH");

const client = new FTPClient(ftpHost, {
  user: user,
  pass: pass,
  mode: "passive",
  port: 21,
});

let articleMap = {};

await client.connect();
await processArticlesFile(client);
await client.close();

async function processArticlesFile(client) {
  let filepath = articleFilepath;
  try {
    let articles = await client.download(filepath);

    const decoder = new TextDecoder();
    let articleText = decoder.decode(articles).split("\n");

    for (let articleLine of articleText) {
      if (articleLine) {
        try {
          let tokens = articleLine.split("|")
          await processArticle(tokens);
        } catch (e) {
          console.log(`Could not process article: ${e.message}`)
        }
      }
    }
  } catch (e) {
    let message = `Unable to download file: ${filepath}, message: ${e.message}`
    console.log(message);
    throw new Error(message)
  }
}

async function processArticle(article) {
  let articleSku = article[0].trim();
  let articleName = article[2];
  let articlePrice = {
    value: Number(article[15].replace(',', '.')) ?? 0.0,
    currency: "U$S",
  };
  let processedArticle = {
    sku: articleSku,
    client_id: clientId,
    merge: false,
    integration_id: clientIntegrationId,
    ecommerce: Object.values(clientEcommerce).map((ecommerce_id) => {
      return {
        ecommerce_id: ecommerce_id,
        properties: [
          { name: articleName },
          {
            price: articlePrice,
          }
        ],
        variants: [],
      };
    }),
  };
  await sagalDispatch(processedArticle);
}
